import React, { createContext, useContext } from 'react';
import CalculatorStore from './CalculatorStore';

const Calculator = new CalculatorStore();

export const CalculatorContext = createContext();

export const CalculatorProvider = ({ children }) => (
	<CalculatorContext.Provider value={Calculator}>
		{children}
	</CalculatorContext.Provider>
);

export const useCalculator = () => useContext(CalculatorContext);