import CalculatorStore, { MAX_INPUT } from './CalculatorStore';

describe('calculator._calculate', () => {
	const calculator = new CalculatorStore();

	test('addition', () => {
		expect(calculator._calculate(10, 5, '+')).toBe(15);
	});
	test('subtraction', () => {
		expect(calculator._calculate(10, 5, '-')).toBe(5);
	});
	test('multiplication', () => {
		expect(calculator._calculate(10, 5, '*')).toBe(50);
	});
	test('division', () => {
		expect(calculator._calculate(10, 5, '/')).toBe(2);
	});
});

describe('calculator._numberPress', () => {
	const calculator = new CalculatorStore();

	test('stores keypresses in method variable in correct order', () => {
		calculator._numberPress('1');
		calculator._numberPress('5');

		expect(calculator.input).toBe('15');
	});

	test('limits input length to max value', () => {
		for(let i = 0; i < MAX_INPUT; i++) {
			calculator._numberPress((i % 10).toString());
		}
		expect(calculator.input.length).toBe(MAX_INPUT);
	});

	test('only 1 decimal point allowed', () => {
		calculator.input = '0';
		calculator._numberPress('1');
		calculator._numberPress('5');
		calculator._numberPress('.');
		calculator._numberPress('5');
		calculator._numberPress('.');
		calculator._numberPress('5');

		expect(calculator.input).toBe('15.55');
	});
});

describe('entering a calculation should work', () => {

	test('basic addition', () => {
		const calculator = new CalculatorStore();
		calculator.buttonPress('1');
		calculator.buttonPress('+');
		calculator.buttonPress('2');
		calculator.buttonPress('=');

		expect(calculator.answer).toBe('3');
	});

	test('series addition', () => {
		const calculator = new CalculatorStore();
		calculator.buttonPress('1');
		calculator.buttonPress('+');
		calculator.buttonPress('2');
		calculator.buttonPress('+');
		calculator.buttonPress('3');
		calculator.buttonPress('=');

		expect(calculator.answer).toBe('6');
	});

	test('sequential addition', () => {
		const calculator = new CalculatorStore();
		calculator.buttonPress('1');
		calculator.buttonPress('+');
		calculator.buttonPress('2');
		calculator.buttonPress('=');
		calculator.buttonPress('+');
		calculator.buttonPress('3');
		calculator.buttonPress('=');

		expect(calculator.answer).toBe('6');
	});
	
});