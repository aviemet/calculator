import { observable, computed, action, toJS } from 'mobx';

export const MAX_INPUT = 30;

class CalculatorStore {
	@observable input = "0"; // The value being entered
	@observable values = []; // All input stored in order
	@observable answer = 0;
	@observable lastKey;
	newInput = false;

	regex = {
		number: /[0-9.]/,
		operator: /[+-/*=]/
	}


	// Return the correct thing to display on the calculator
	@computed get display() { 
		return this.values.length > 2 && this.regex.operator.test(this.lastKey) ? this.answer : this.input; 
	}

	// Return the full calculation to be displayed
	@computed get history() { return this.values.slice(0, -1).join(' '); }

	// Return the last value entered
	@computed get _lastValue() { return this.values ? this.values[this.values.length-1] : null; } 

	// Perform the calculation on the series of inputs
	@action
	_calculateAnswer() {
		if(this.values.length > 2) {
			let total = this.values[0];

			for(let i = 1; i < this.values.length; i += 2) {
				let left = parseFloat(total);
				let operator = this.values[i];
				let right = this.values.length > i+1 ? parseFloat(this.values[i+1]) : false;

				if(right) {
					total = this._calculate(left, right, operator);
				}
			}
			
			return (total || 0).toString();
		}
	}

	// Take 2 numbers and a string operator to perform a calculation
	_calculate(left, right, operator) {
		switch (operator) {
			case '+':
				return left + right;
			case '-':
				return left - right;
			case '*':
				return left * right;
			case '/':
				return left / right;
			default:
		}
	}

	// Delegate button presses between numbers and operators
	@action
	buttonPress(key) {
		const mappedKey = this._mapKeyboardToFn(key);
		if(this.values[this.values.length-1] === '=' && /[0-9.`]/.test(key)) {
			this.values = [];
		}

		if(/[0-9.]/.test(mappedKey)) {
			this._numberPress(mappedKey.toString());
		} else {
			this._functionKeyPress(mappedKey);
			this.answer = this._calculateAnswer();
		}
		this.lastKey = mappedKey;
	}

	// Action to perform when a number is pressed
	@action
	_numberPress(key) {
		// Clear the input value after an operator was used
		if(this.lastKey && !/[0-9.`]/.test(this.lastKey)) this.input = "0";

		// Limit the length of the input value
		if(this.input.length >= MAX_INPUT) return;

		// Record the value input
		if(/[0-9]/.test(key)) {
			if(this.input === "0") {
				this.input = key;
			} else {
				this.input += key;
			}
		// Limit to only 1 decimal point
		} else if(!/[.]/.test(this.input)) {
			this.input += key;
		}
	}

	// Action to perform when a function key is pressed
	@action
	_functionKeyPress(key) {
		this.newInput = true;

		switch (key) {
			case 'C':
				if(this.input !== "0") {
					this.input = "0";
				} else {
					this.answer = 0;
					this.values = [];
				}
				break;
			case '`':
				this.input = (parseFloat(this.input) * -1).toString();
				break;
			case '=':
			default:
				// Replace the last operator 
				if(!/[0-9.`]/.test(this.lastKey) && !/[0-9.`]/.test(this._lastValue)) {
					if(this._lastValue !== '=') {
						this.values[this.values.length-1] = key;
					} else {
						this.values = [];
						this.values.push(this.answer);
						this.values.push(key);
					}
				// Put the value and operator on the queue
				} else {
					this.values.push(this.input);
					this.values.push(key);
				}
		}
	}

	// Convert keyboard input value codes to normalized calculator command value
	_mapKeyboardToFn(key) {
		switch (key) {
			case "Escape":
				return "C";
			case "Enter":
				return "=";
			default:
				return key;
		}
	}
}

export default CalculatorStore;

