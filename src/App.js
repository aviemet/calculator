import React from 'react';

import styled, { ThemeProvider } from 'styled-components';
import theme from './theme';

import Calculator from './Calculator';
import { CalculatorProvider } from './Context/CalculatorContext';

const Container = styled.div`
  display: block;
  position: relative;
  top: 50%;
  transform: translateY(-50%);
`;

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CalculatorProvider>
        <Container>
          <Calculator />
        </Container>
      </CalculatorProvider>
    </ThemeProvider>
  );
}

export default App;
