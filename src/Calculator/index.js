import React from 'react'
import Display from './Display';

import Button from '../Components/Button';
import NumberButton from '../Components/NumberButton';
import styled from 'styled-components';

const CalculatorChrome = styled.div`
	background: black;
	border-radius: ${props => props.theme.borderRadius} ${props => props.theme.borderRadius} 0 0;
	margin: 0 auto;
	width: 400px;
`;

const Buttons = styled.div`
	padding: 1px;
	display: grid;
	grid-template-columns: repeat(4, 1fr);
	grid-gap: 1px;
`;

const Calculator = () => {	
	return (
		<CalculatorChrome>
			<Display />
			<Buttons>
				<Button value='C'>AC</Button>
				<Button value='`'>+/-</Button>
				<Button value='%'>%</Button>
				<Button value='/'>&#247;</Button>

				<NumberButton value={7} />
				<NumberButton value={8} />
				<NumberButton value={9} />
				<Button value='x'>X</Button>

				<NumberButton value={4} />
				<NumberButton value={5} />
				<NumberButton value={6} />
				<Button value='-'>-</Button>

				<NumberButton value={1} />
				<NumberButton value={2} />
				<NumberButton value={3} />
				<Button value='+'>+</Button>

				<NumberButton
					value={0}
					style={{
						gridColumn: '1 / 3'
					}} 
				/>
				<Button value='.'>.</Button>
				<Button value='='>=</Button>

			</Buttons>
		</CalculatorChrome>
	);
}

export default Calculator;