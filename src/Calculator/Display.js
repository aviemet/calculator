import React, { useState, useEffect, useContext } from 'react';

import styled from 'styled-components';
import { useCalculator } from '../Context/CalculatorContext';
import { observer } from 'mobx-react-lite';

const DisplayContainer = styled.div`
	width: 100%;
	border-bottom: 1px solid #666;
	border-radius: ${props => props.theme.borderRadius} ${props => props.theme.borderRadius} 0 0

	background: #333;
	color: #EEE;
`;

const DisplayInput = styled.div`
	text-align: right;
	font-size: 2.6rem;
	padding: 5px 10px;
	height: 45px;
`;

const DisplayValue = styled.div`
	text-align: right;
	font-size: 1.2rem;
	padding: 5px 10px;
	height: 25px;
`;

const Display = observer(() => {
	const calculator = useCalculator();
	
  const handleKeyUp = e => {
		const key = e.key || e.keyCode;
		calculator.buttonPress(key);
	}

  useEffect(() => {
    document.addEventListener('keyup', handleKeyUp)
  }, []);

	return (
		<DisplayContainer>
			<DisplayInput>{calculator.display}</DisplayInput>
			<DisplayValue>{calculator.history}</DisplayValue>
		</DisplayContainer>
	);
});

export default Display;