import React from 'react';

import Button from './Button';

const NumberButton = props => {
	
	return (
		<Button {...props}>
			{props.value}
		</Button>
	);
};

export default NumberButton;