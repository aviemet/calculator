import React from 'react';
import { useCalculator } from '../Context/CalculatorContext';
import { observer } from 'mobx-react-lite';

import styled from 'styled-components';

const BasicButton = styled.button.attrs(({ style }) => ({ ...style }))` 
	width: 100%;
	height: 50px;
	background: #e4cc66;
	color: black;
	text-align: center;
	font-size: 2rem;
	display: inline-block;
	border: none;

	&:focus {
		outline: none;
	}
`;

const Button = observer((props) => {
	const calculator = useCalculator();
	return (
		<BasicButton {...props} 
		
		onClick={() => calculator.buttonPress(props.value)}>
			{props.children}
		</BasicButton>
	);
});

export default Button;